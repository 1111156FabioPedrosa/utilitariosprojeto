/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listaLigada;

/**
 *
 * @author fabio
 */
public class No {
    Object elemento;
    No prox;

    public No(Object elemento, No prox) {
        setElemento(elemento);
        setProx(prox);
    }

    public No() {
        this(null, null);
    }
    
   public No(No no) {
       this(no.elemento, no.prox);
   }
    
    public Object getElemento() {
        return elemento;
    }

    public No getProx() {
        return prox;
    }

    public void setElemento(Object elemento) {
        this.elemento = elemento;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }

    @Override
    public String toString() {
        return this.elemento.toString();
    }
    
    
}
